const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const prizeHistoryModel = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    user:{
            type: mongoose.Types.ObjectId,
            ref: 'User',
            required: true,
    },
    prize:{
            type: mongoose.Types.ObjectId,
            ref: 'Prize',
            required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    updatedAt: {
        type: Date,
        default: Date.now(),
    }
});

module.exports = mongoose.model('PrizeHistory', prizeHistoryModel);
