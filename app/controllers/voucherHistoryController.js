const mongoose = require('mongoose');
const voucherHistoryModel = require('../models/voucherHistoryModel');
const voucherModel = require('../models/voucherModel');
const userModel = require('../models/userModel');

const voucherHistoryController = {
    //POST new voucher history
    createVoucherHistory: (req,res) => {
        //B1: cbi dữ liệu
        let bodyReq = req.body;
        let newVoucherHistory = {
            _id: mongoose.Types.ObjectId(),
            user: bodyReq.user,
            voucher: bodyReq.voucher
        }
        //B2: kiểm tra dữ liệu
        if (!(bodyReq.user || bodyReq.voucher)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'User or Prize are invalid'
            })
        };

        //B3: thao tác với CSDL
        voucherHistoryModel.create(newVoucherHistory, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error : ${err.message}`
                })
            } else {
                return res.status(201).json({
                    status: 'New Voucher History has been created!',
                    'New Voucher History': data
                })
            }
        })
    },

    //GET all vouchers history
    getAllVouchersHistory: (req,res) => {
         //B1: cbi dữ liệu
         let userId = req.query.user;
         let condition = {};
         if (userId) {
             condition.user = userId
         };
         console.log(condition);
         
        voucherHistoryModel.find(condition, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal Server error : ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET All vouchers history',
                    'Voucher History List': data
                })
            }
        }).populate('voucher');
    },

    //GET a voucher history by ID
    getVoucherHistoryById: (req,res) => {
        //B1: cbi dữ liệu
        let voucherHistoryId = req.params.voucherHistoryId;
        //B2: ktra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
            return res.status(400).json({
                message: `Voucher History ID is invalid`
            })
        } else {
            voucherHistoryModel.findById(voucherHistoryId, (err,data) => {
                if (err) {
                    return res.status(500).json({
                        message: `Internal Server error : ${err.message}`
                    })
                } else {
                    return res.status(200).json({
                        status: 'GET a voucher history',
                        'Voucher History Found': data
                    })
                }
            })
        }
    },

    //GET voucher history by Username
    getVoucherHistoryByUsername: (req,res) => {
        //B1: cbi dữ liệu
        let username = req.query.username;
        console.log(username);
        //B2: ktra dữ liệu
        if (!username) {
             voucherHistoryModel.find( (err,data) => {
                 if (err) {
                     return res.status(500).json({message: `Internal server error: ${err.message}`})
                 } else {
                     return res.status(200).json({
                         status: 'GET all vouchers histories',
                         'Voucher Histories List': data
                     })
                 }
             })
        } else {
         userModel.findOne({ username }, (errFind, userExist) => {
             if (errFind) {
                voucherHistoryModel.find( (err,data) => {
                     if (err) {
                         return res.status(500).json({message: `Internal server error: ${err.message}`})
                     } else {
                         return res.status(200).json({
                             status: 'GET all vouchers histories',
                             'Vouchers Histories List': data
                         })
                     }
                 })
             } else {
                if (!userExist) {
                   return res.status(200).json({
                        status: 'Username not found',
                        'Voucher History': []
                   })
                } else {
                    voucherHistoryModel.find( { user : userExist._id }, (err,data) => {
                        if (err) {
                            return res.status(500).json({message: `Internal server error: ${err.message}`})
                        } else {
                            console.log(userExist._id)
                            return res.status(200).json(data)
                        }
                    }).populate('voucher');
                }
             }
         })
        }
     },

    //PUT a voucher history by ID
    updateVoucherHistoryById: (req,res) => {
        //B1: cbi dữ liệu
        let voucherHistoryId = req.params.voucherHistoryId;
        let bodyReq = req.body;
        let updateVoucherHistory = {
            user: bodyReq.user,
            voucher: bodyReq.voucher
        }
        //B2: ktra dữ liệu
        if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
            return res.status(400).json({
                message: `Voucher History ID is invalid`
            })
        } else {
        //B3: thao tác với CSDL
            voucherHistoryModel.findByIdAndUpdate(voucherHistoryId, updateVoucherHistory, (err,data) => {
                if (err) {
                    return res.status(500).json({
                        message: `Internal server error : ${err.message}`
                    })
                } else {
                    return res.status(200).json({
                        status: 'Voucher History has been updated!',
                        'Voucher History': data,
                        'Data updated': updateVoucherHistory
                    })
                }
            })
        }
    },

    //DELETE a voucher history by ID
    deleteVoucherHistoryById: (req,res) => {
        //B1: cbi dữ liệu
        let voucherHistoryId = req.params.voucherHistoryId;
         //B2: ktra dữ liệu
         if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
            return res.status(400).json({
                message: `Voucher History ID is invalid`
            })
        } else {
        //B3: thao tác với CSDL
        voucherHistoryModel.findByIdAndDelete(voucherHistoryId, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error : ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'Voucher History has been deleted!',
                })
            }
            })
        }
    }

};

module.exports = voucherHistoryController;
