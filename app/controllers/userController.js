const mongoose = require('mongoose');
const UserModel = require('../models/userModel');

const UserController = {
    createUser: (req,res) => {
        //B1:
        let bodyReq = req.body;
        let newUser = {
            _id: mongoose.Types.ObjectId(),
            username: bodyReq.username,
            firstname: bodyReq.firstname,
            lastname: bodyReq.lastname,
        }
        //B2: 
        //B3: thao tác với CSDL
        UserModel.create(newUser, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message:`Internal server error: ${err.message}`
                })
            }else {
                return res.status(201).json({
                    'New User': data
                })
            }
        })
    },

    getAllUsers: (req,res) => {
        UserModel.find((err,data)=> {
            if(err) {
                return res.status(500).json({
                    message:`Internal server error: ${err.message}` 
                })
            }else {
                return res.status(200).json({
                    'All Users': data
                })
            }
        })
    },

    getUserById: (req,res) => {
        //B1 :
        let userId = req.params.userId;
        //B2 :
        if (!mongoose.Types.ObjectId.isValid(userId)) {
            return res.status(400).json({
                message: `ERROR 400: Bad Request`
            })
        //B3 :
        } else {
            UserModel.findById(userId, (err,data) => {
                if (err) {
                    return res.status(500).json({
                        message:`Internal server error: ${err.message}`
                    });
                } else {
                    return res.status(200).json({
                        'User found': data
                    })
                }
            })
        }
    },

    updateUserById: (req,res) => {
        //B1 :
        let userId = req.params.userId;
        let bodyReq = req.body;
        let userUpdated = {
            username: bodyReq.username,
            firstname: bodyReq.firstname,
            lastname: bodyReq.lastname
        }
        //B2 :
        //B3 : 
        UserModel.findByIdAndUpdate(userId, userUpdated, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message:`Internal server error: ${err.message}`      
                })
            } else {
                return res.status(200).json({
                    'User Updated': data,
                    'Data Updated': userUpdated
                })
            }
        })
    },

    deleteUserById: (req,res) => {
         //B1 :
         let userId = req.params.userId;
         //B2 : 
         if (!mongoose.Types.ObjectId.isValid(userId)) {
            return res.status(400).json({
                message: `ERROR 400: Bad Request`
            })
        //B3 :
         }
        UserModel.findByIdAndDelete(userId, (err,data) => {
                if (err) {
                    return res.status(500).json({
                        message:`Internal server error: ${err.message}`  
                    })
                } else {
                    return res.status(204).json({
                        message: `User has been deleted`
                    })
                }
            })
    }

};

module.exports = UserController;
