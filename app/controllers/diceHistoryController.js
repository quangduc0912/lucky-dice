const mongoose = require('mongoose');
const diceHistoryModel = require('../models/diceHistoryModel');
const userModel = require('../models/userModel');

const diceHistoryController = {
    //POST 
    createDiceHistory: (req,res) => {
        //B1: 
        let randomNumber = Math.floor(Math.random()* 6) + 1;
        let bodyReq = req.body;
        let newDiceHistory = {
            _id: mongoose.Types.ObjectId(),
            user: bodyReq.user,
            dice: randomNumber
        }
        //B2:
        // if (!(bodyReq.dice >= 0 && bodyReq.dice <= 6 && Number.isInteger(bodyReq.dice))) {
        //     return res.status(400).json({
        //         status: 'ERROR 400: Bad Request',
        //         message: 'Dice number must be > 0 and <= 6'
        //     })
        // }
        //B3: 
        diceHistoryModel.create(newDiceHistory, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `ERROR 500: Internal server error :${err.message}`
                })
            } else {
                return res.status(201).json({
                    status: 'New Dice History created!',
                    data
                })
            }
        })
    },

    //GET all dices history
    getAllDices: (req,res) => {
        //B1: cbi dữ liệu
        let userId = req.query.user;
        let condition = {};
        if (userId) {
            condition.user = userId
        };
        console.log(condition);
        diceHistoryModel.find(condition, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `ERROR 500: Internal server error :${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET All Dices Histories',
                    data
                })
            }
        })
    },

    //GET a dice history by ID
    getDiceHistoryById: (req,res) => {
        //B1: 
        let diceHistoryId = req.params.diceHistoryId;
        //B2: 
        if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Dice History ID is invalid!'
            })
        }
        //B3: 
        diceHistoryModel.findById(diceHistoryId, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `ERROR 500: Internal server error :${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'Get Dice History!',
                    data
                })
            }
        })
    },

    //GET dice history by Username
    getDiceHistoryByUsername: (req,res) => {
        //B1: cbi dữ liệu
        let username = req.query.username;
        console.log(username);
        //B2: ktra dữ liệu
        if (!username) {
             diceHistoryModel.find( (err,data) => {
                 if (err) {
                     return res.status(500).json({message: `Internal server error: ${err.message}`})
                 } else {
                     return res.status(200).json({
                         status: 'GET all dices histories',
                         'Dice Histories List': data
                     })
                 }
             })
        } else {
         userModel.findOne({ username }, (errFind, userExist) => {
             if (errFind) {
                diceHistoryModel.find( (err,data) => {
                     if (err) {
                         return res.status(500).json({message: `Internal server error: ${err.message}`})
                     } else {
                         return res.status(200).json({
                             status: 'GET all dices histories',
                             'Dice Histories List': data
                         })
                     }
                 })
             } else {
                if (!userExist) {
                   return res.status(200).json({
                        status: 'Username not found',
                        'Dice History': []
                   })
                } else {
                    diceHistoryModel.find( { user : userExist._id }, (err,data) => {
                        if (err) {
                            return res.status(500).json({message: `Internal server error: ${err.message}`})
                        } else {
                            console.log(username._id)
                            return res.status(200).json(data)
                        }
                    })
                }
             }
         })
        }
     },

    //PUT a dice history by ID
    updateDiceHistoryById: (req,res) => {
        //B1: 
        let diceHistoryId = req.params.diceHistoryId;
        let bodyReq = req.body;
        let updateDiceHistory = {
            user: bodyReq.user,
            dice: bodyReq.dice
        }
        //B2: 
        if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Dice History ID is invalid!'
            })
        };

        if (!(bodyReq.dice >= 0 && bodyReq.dice <= 6 && Number.isInteger(bodyReq.dice))) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Dice number must be > 0 and <= 6'
            })
        };
        //B3:
        diceHistoryModel.findByIdAndUpdate(diceHistoryId, updateDiceHistory, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message:`Internal server error: ${err.message}`      
                })
            } else {
                return res.status(200).json({
                    'Dice History Updated': data,
                    'Data Updated': updateDiceHistory
                })
            }
        })
    },

    deleteDiceHistoryById: (req,res) => {
        //B1 :
        let diceHistoryId = req.params.diceHistoryId;
        //B2 : 
        if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Dice History ID is invalid!'
            })
       //B3 :
        }
       diceHistoryModel.findByIdAndDelete(diceHistoryId, (err,data) => {
               if (err) {
                   return res.status(500).json({
                       message:`Internal server error: ${err.message}`  
                   })
               } else {
                   return res.status(204).json({
                       message: `User has been deleted`
                   })
               }
           })
    }

}

module.exports = diceHistoryController; 
