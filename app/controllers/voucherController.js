const mongoose = require('mongoose');
const voucherHistoryModel = require('../models/voucherHistoryModel');
const VoucherModel = require('../models/voucherModel');

const VoucherController = {
    //POST a voucher
    createVoucher: (req,res) => {
        //B1:
        let bodyReq = req.body;
        let newVoucher = {
            _id: mongoose.Types.ObjectId(),
            code: bodyReq.code,
            discount: bodyReq.discount,
            note: bodyReq.note
        }
        //B2: 
        if (!(bodyReq.discount >= 0 && Number.isInteger(bodyReq.discount) && bodyReq.discount < 100)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Discount must be >= 0 and < 100'
            })
        };
        //B3:
        VoucherModel.create(newVoucher, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error : ${err.message}`
                })
            } else {
                return res.status(201).json({
                    status: 'New Voucher has been created!',
                    data
                })
            }
        })
    },

    //GET all vouchers
    getAllVouchers: (req,res) => {
        //B1:
        //B2:
        //B3:
        VoucherModel.find( (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error : ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET all vouchers successfully!',
                    data
                })
            }
        })
    },

    //GET a voucher by ID
    getVoucherById: (req,res) => {
        //B1:
        let voucherId = req.params.voucherId;
        //B2:
        if (!mongoose.Types.ObjectId.isValid(voucherId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Voucher ID is invalid!'
            })
        };
        //B3:
        VoucherModel.findById(voucherId, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error : ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'GET voucher successfully!',
                    'Voucher Found': data
                })
            }
        })
    },

    //PUT a voucher by ID
    updateVoucherById: (req,res) => {
        //B1:
        let voucherId = req.params.voucherId;
        let bodyReq = req.body
        let updateVoucher = {
            code: bodyReq.code,
            discount: bodyReq.discount,
            note: bodyReq.note
        }
        //B2:
        if (!mongoose.Types.ObjectId.isValid(voucherId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Voucher ID is invalid!'
            })
        };

        if (!(bodyReq.discount >= 0 && Number.isInteger(bodyReq.discount) && bodyReq.discount < 100)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Discount must be >= 0 and < 100'
            })
        };
        //B3:
        VoucherModel.findByIdAndUpdate(voucherId, updateVoucher, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error : ${err.message}`
                })
            } else {
                return res.status(200).json({
                    status: 'Update voucher successfully!',
                    'Voucher Found': data,
                    'Data Update': updateVoucher
                })
            }
        })
    },

    //DELETE a voucher by ID
    deleteVoucherById: (req,res) => {
        //B1:
        let voucherId = req.params.voucherId;
        //B2: 
        if (!mongoose.Types.ObjectId.isValid(voucherId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Voucher ID is invalid!'
            })
        };
        //B3: 
        VoucherModel.findByIdAndDelete(voucherId, (err,data) => {
            if (err) {
                return res.status(500).json({
                    message: `Internal server error : ${err.message}`
                })
            } else {
                return res.status(204).json({
                    status: 'Delete voucher successfully!',
                })
            }
        })
    }
};

module.exports = VoucherController;
