const diceRouter = require('express').Router();
const { diceHandler } = require('../controllers/diceController');
const diceHistoryController = require('../controllers/diceHistoryController');
const prizeHistoryController = require('../controllers/prizeHistoryController');
const voucherHistoryController = require('../controllers/voucherHistoryController');


diceRouter.post('/dice', diceHandler);

diceRouter.get('/dice-history', diceHistoryController.getDiceHistoryByUsername);

diceRouter.get('/prize-history', prizeHistoryController.getPrizeHistoryByUsername);

diceRouter.get('/voucher-history', voucherHistoryController.getVoucherHistoryByUsername);



module.exports = diceRouter;
