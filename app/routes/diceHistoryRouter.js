const DiceHistoryRouter = require('express').Router();
const diceHistoryController = require('../controllers/diceHistoryController');

DiceHistoryRouter.post('/dice-histories', diceHistoryController.createDiceHistory);

DiceHistoryRouter.get('/dice-histories', diceHistoryController.getAllDices);

DiceHistoryRouter.get('/dice-histories/:diceHistoryId', diceHistoryController.getDiceHistoryById);

DiceHistoryRouter.put('/dice-histories/:diceHistoryId', diceHistoryController.updateDiceHistoryById)

DiceHistoryRouter.delete('/dice-histories/:diceHistoryId', diceHistoryController.deleteDiceHistoryById)

module.exports = DiceHistoryRouter;
