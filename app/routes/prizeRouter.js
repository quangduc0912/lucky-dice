const PrizeRouter = require('express').Router();
const PrizeController = require('../controllers/prizeController');

PrizeRouter.post('/prizes', PrizeController.createPrize);

PrizeRouter.get('/prizes', PrizeController.getAllPrizes);

PrizeRouter.get('/prizes/:prizeId', PrizeController.getPrizeById);

PrizeRouter.put('/prizes/:prizeId', PrizeController.updatePrizeById);

PrizeRouter.delete('/prizes/:prizeId', PrizeController.deletePrizeById);

module.exports = PrizeRouter;
