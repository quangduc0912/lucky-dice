const VoucherHistoryRouter = require('express').Router();
const voucherHistoryController = require('../controllers/voucherHistoryController');


VoucherHistoryRouter.post('/vouchers-histories', voucherHistoryController.createVoucherHistory);

VoucherHistoryRouter.get('/vouchers-histories', voucherHistoryController.getAllVouchersHistory);

VoucherHistoryRouter.get('/vouchers-histories/:voucherHistoryId', voucherHistoryController.getVoucherHistoryById);

VoucherHistoryRouter.put('/vouchers-histories/:voucherHistoryId', voucherHistoryController.updateVoucherHistoryById);

VoucherHistoryRouter.delete('/vouchers-histories/:voucherHistoryId', voucherHistoryController.deleteVoucherHistoryById);


module.exports = VoucherHistoryRouter;
